/**Mobile Nav Action */
let mainNav = document.getElementById("js-menu");
let navBarToggle = document.getElementById("js-navbar-toggle");
let navBarClose = document.getElementById("js-navbar-close");
let navWrapper = document.querySelector(".mobile-nav-wrapper");
// let locationBtn = document.querySelector("#location");

navBarToggle.addEventListener("click", function () {
  mainNav.classList.toggle("active");

  if (mainNav.className.split(" ").includes("active")) {
    navWrapper.classList.remove("displayNone");
  } else {
    navWrapper.classList.add("displayNone");
  }
});

navBarClose.addEventListener("click", function () {
  mainNav.classList.remove("active");

  if (mainNav.className.split(" ").includes("active")) {
    navWrapper.classList.remove("displayNone");
  } else {
    navWrapper.classList.add("displayNone");
  }
});

(function () {
  var $ = jQuery || undefined;
  if ($) {
    if ($(".gallery").is(":visible")) {
      $(".gallery").empty();

      let imgMarkup = ``;

      for (let i = 0; i < 31; i++) {
        imgMarkup =
          imgMarkup +
          `<a class="gallery__link">
        <figure class="gallery__thumb">
            <img src="/img/renamed/wedding_${i + 1}.jpg" data-num=${
            i + 1
          } alt="#khushraaaj"
                class="gallery__image">
            <figcaption class="gallery__caption"></figcaption>
        </figure>
    </a>`;
      }

      $(".gallery").html(` <div class="gallery__column">${imgMarkup}</div>`);
    }
  }
})();

var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.querySelector("img");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");

$("img").click(function () {
  let src = this;
  let num = this.dataset.num;
  modalImg.src = this.src;
  modal.style.display = "block";
  modalImg.src = `/img/og_img/wedding_${num}.jpg`;
  captionText.innerHTML = this.alt;
});
// img.onclick = function () {
//   modal.style.display = "block";
//   modalImg.src = this.src;
//   captionText.innerHTML = this.alt;
// };

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
};

// function returnMapUrl(GLatLngObj) {
//   var point = GLatLngObj || map.getCenter();
//   return "http://maps.google.com/?ll=" + point.lat + "," + point.lng;
// }

// locationBtn.addListener("click", function (overlay, latlng) {
//   window.location = `https://g.page/InterfaceBanquetsMalad?share`;
// });
